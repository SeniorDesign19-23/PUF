package dataTypes;

import java.util.ArrayList;
import java.util.List;


public class Utils {
	
	String testerName = "";
	String deviceName = "";
	private static List<List<Double>> challengeList;
	static List<List<Double>> respList;
	static List<Double> pressureList = new ArrayList<>();
	
	public Utils(String fileName, String testerName, String deviceName, List<List<Double>> challengeList, List<List<Double>> respone_list, List<Double> pressureList){
		this.testerName = testerName;
		this.deviceName = deviceName;
		Utils.challengeList = challengeList;
		Utils.respList = respone_list;
		Utils.pressureList = pressureList;
	}
	
    public static ArrayList<Float> interpolatedPressure() {
    	    	
		ArrayList<Float> normalizedPressureList = new ArrayList<>();

    	
        float challengeLength = 0;
        float distributedLength = 0;
        boolean isX = false;
        	
        for(int i = 0; i < challengeList.size() -1 ; i++) {
        	float yLength = (float) Math.abs(challengeList.get(i).get(1) - challengeList.get(i+1).get(1));
        	float xLength = (float) Math.abs(challengeList.get(i).get(0) - challengeList.get(i+1).get(0));
        		
        	if(xLength > yLength) {
        		challengeLength = xLength;
        		isX = true;
        	}
        	else {
        		challengeLength = yLength;
        		isX = false;
        	}
        }
        	
        distributedLength = (float) (challengeLength/32.0);
    		
    	ArrayList<List<Double>> normalizedList = new ArrayList<>();
    	List<Double> currPt = challengeList.get(0);
    	
    		
    	for(int i = 0; i < 32; i++) {
    		normalizedList.add(new ArrayList<Double>(currPt));
    		if(isX) {
    			currPt.set(0,currPt.get(0) + distributedLength);
    		}
    		else {
    			currPt.set(1, currPt.get(1) + distributedLength);
    		}
    	}
    		
    	System.out.println(respList);
    	System.out.println(pressureList);
    		
    	for(List<Double> normPt: normalizedList) {
    			if(isX) {
    				List<Double> closestLeftPt = respList.get(0);
    				List<Double> closestRightPt = respList.get(respList.size()-1);
    				int closestLeftPtInd = 0;
    				int closestRightPtInd = respList.size() -1;
    					
    				if(respList.get(0).get(0) > normPt.get(0)) {
    					closestLeftPt = respList.get(respList.size() - 1);
    					closestRightPt = respList.get(0);
    					closestLeftPtInd = respList.size() -1;
   						closestRightPtInd = 0;
   					}
    					
   					for (int i = 0; i < respList.size(); i++) {
   						if(respList.get(i).get(0) <= normPt.get(0)) { 
    						if(normPt.get(0) - respList.get(i).get(0) < normPt.get(0) - closestLeftPt.get(0)) {
    							closestLeftPt = respList.get(i);
    							closestLeftPtInd = i;
    						}
    					}
    					if(respList.get(i).get(0) > normPt.get(0)) {
    						if(respList.get(i).get(0) - normPt.get(0) < closestRightPt.get(0) - normPt.get(0)) {
    							closestRightPt = respList.get(i);
    							closestRightPtInd = i;
    						}
    					}
   					}
    					
   					if(closestRightPt.equals(closestLeftPt)) {
   						closestLeftPtInd = closestLeftPtInd - 1;
   						closestLeftPt = respList.get(closestLeftPtInd);
   					}
    					
    				float deltaX = (float) (closestRightPt.get(0) - closestLeftPt.get(0));
    				float deltaY = (float) (closestRightPt.get(1) - closestLeftPt.get(1));
    				float angle = (float) Math.abs(Math.atan((double )(deltaY)/deltaX));
    				float deltaX1 = (float) (normPt.get(0) - closestLeftPt.get(0));
    				float h1 = (float) (deltaX1 / Math.cos((double) angle));
    					
    				float h = (float) (deltaX / Math.cos((double) angle));
   					float h2 = h - h1;
    					
   					float interpPress = (float) ((h1/h)*pressureList.get(closestLeftPtInd) + (h2/h)*pressureList.get(closestRightPtInd));
   					normalizedPressureList.add(interpPress);
   				}
    				
    			else {
    				List<Double> closestLowerPt = respList.get(0);
    				List<Double> closestUpperPt = respList.get(respList.size()-1);
    				int closestLowerPtInd = 0;
    				int closestUpperPtInd = respList.size() -1;
    					
    				if(respList.get(0).get(1) > normPt.get(1)) {
    					closestLowerPt = respList.get(respList.size() -1);
    					closestUpperPt = respList.get(0);
    					closestLowerPtInd = respList.size() -1;
    					closestUpperPtInd = 0;
    				}
    					
    				for(int i = 0; i < respList.size(); i++) {
    					if(respList.get(i).get(1) <= normPt.get(1)) { //if its below
    						if(normPt.get(1) - respList.get(i).get(1) < normPt.get(1) - closestLowerPt.get(1)) {
    							closestLowerPt = respList.get(i);
    							closestLowerPtInd = i;
    						}
    						}
    					if(respList.get(i).get(1) > normPt.get(1)) {// if its above
    						if(respList.get(i).get(1) - normPt.get(1) < closestUpperPt.get(1) - normPt.get(1)) {
    							closestUpperPt = respList.get(i);
    							closestUpperPtInd = i;
    						}
    						}
    				}
    					
    				if(closestUpperPt.equals(closestLowerPt)) {
    					closestLowerPtInd = closestLowerPtInd - 1;
    					closestLowerPt = respList.get(closestLowerPtInd);
    				}
    					
    				float deltaX = (float) (closestUpperPt.get(0) - closestLowerPt.get(0));
    				float deltaY = (float) (closestUpperPt.get(1) - closestLowerPt.get(1));
    				float angle = (float) Math.abs(Math.atan((double )(deltaX)/deltaY));
    				float deltaY1 = (float) (normPt.get(1) - closestLowerPt.get(1));
    				float h1 = (float) (deltaY1 / Math.cos((double) angle));
    					
    				float h = (float) (deltaY / Math.cos((double) angle));
    				float h2 = h - h1;
    					
    				float interpPress = (float) ((h1/h)*pressureList.get(closestLowerPtInd) + (h2/h)*pressureList.get(closestUpperPtInd));
    				normalizedPressureList.add(interpPress);
    					
    			}
    	}
    	return normalizedPressureList;
    }
	
}