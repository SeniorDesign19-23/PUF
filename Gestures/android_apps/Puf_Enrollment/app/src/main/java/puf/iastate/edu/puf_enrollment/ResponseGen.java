
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;


public class ResponseGen {

	final static String CSV_OUTPUT_DIR = "/home/ammar/Desktop/CSV/";
	final static String GENERATED_OUTPUT_DIR = "/home/ammar/Desktop/OutputCSV/";
	
    public static void main(String[] args) throws IOException {
    	
    	//File[] files = new File(CSV_OUTPUT_DIR).listFiles(File::isDirectory);
    	
    	ArrayList<CustomPoint> challengeList = new ArrayList<CustomPoint>();
    	ArrayList<Float> pressureList = new ArrayList<Float>();
    	ArrayList<CustomPoint> respList = new ArrayList<CustomPoint>();
    	String testerName = "";
    	String deviceName = "";
    	boolean responseStarted = false;
    	Scanner fileReader;
    	Scanner rowReader;
    	List<Path> filesList = Files.walk(Paths.get(CSV_OUTPUT_DIR))
    						.filter(Files::isRegularFile)
    						.collect(Collectors.toList());
    	//for(int i = 0; i < files.length; i++) {
    		//System.out.println(files[i]);
    	//}
    	
    	for(int a = 0; a < filesList.size(); a++) {
    		fileReader = new Scanner(filesList.get(a));
    		while(fileReader.hasNext()) {
    			rowReader = new Scanner(fileReader.nextLine());
    			rowReader.useDelimiter(",");
    			ArrayList<String> rowElements = new ArrayList<String>();
    			
    			while (rowReader.hasNext()) {
    				String element = rowReader.next();
    				element = element.replaceAll("\"","");
    				//element = element.substring(0, element.length());
    				rowElements.add(element);
    			}
    			
    			if(responseStarted) {
    				CustomPoint thisPoint = new CustomPoint(Float.parseFloat(rowElements.get(0)), Float.parseFloat(rowElements.get(1)));
    				respList.add(thisPoint);
    				pressureList.add(Float.parseFloat(rowElements.get(2)));
    			}
    			else if(!rowElements.get(0).equals("ChallengeX") && !rowElements.get(0).equals("X")) {
    				CustomPoint thisPoint = new CustomPoint(Float.parseFloat(rowElements.get(0)), Float.parseFloat(rowElements.get(1)));
    				challengeList.add(thisPoint);
    				testerName = rowElements.get(2);
    				deviceName = rowElements.get(3);
    			}
    			if(rowElements.contains("X") && rowElements.contains("Y") && rowElements.contains("PRESSURE")) {
    				responseStarted = true;
    				int x = 0;
    				while(x < rowElements.size()) {
        				System.out.println(rowElements.get(x));
        				x++;
    				}    			
    			}
    			

    		}
    		
        	float challengeLength = 0;
        	float distributedLength = 0;
        	boolean isX = false;
        	
        	for(int i = 0; i < challengeList.size() -1 ; i++) {
        		float yLength = Math.abs(challengeList.get(i).getY() - challengeList.get(i+1).getY());
        		float xLength = Math.abs(challengeList.get(i).getX() - challengeList.get(i+1).getX());
        		
        		if(xLength > yLength) {
        			challengeLength = xLength;
        			isX = true;
        		}
        		else {
        			challengeLength = yLength;
        			isX = false;
        		}
        	}
        	
        	distributedLength = (float) (challengeLength/32.0);
    		
    		ArrayList<CustomPoint> normalizedList = new ArrayList<>();
    		CustomPoint currPt = challengeList.get(0);
    		
    		for(int i = 0; i < 32; i++) {
    			normalizedList.add(currPt);
    			if(isX) {
    				currPt = new CustomPoint(currPt.getX() + distributedLength, currPt.getY());
    			}
    			else {
    				currPt = new CustomPoint(currPt.getX(), currPt.getY() + distributedLength);
    			}
    		}
    		
    		ArrayList<Float> normalizedPressureList = new ArrayList<>();
    		System.out.println(respList);
    		System.out.println(pressureList);
    		
    		for(CustomPoint normPt: normalizedList) {
    				if(isX) {
    					CustomPoint closestLeftPt = respList.get(0);
    					CustomPoint closestRightPt = respList.get(respList.size()-1);
    					int closestLeftPtInd = 0;
    					int closestRightPtInd = respList.size() -1;
    					
    					if(respList.get(0).getX() > normPt.getX()) {
    						closestLeftPt = respList.get(respList.size() - 1);
    						closestRightPt = respList.get(0);
    						closestLeftPtInd = respList.size() -1;
    						closestRightPtInd = 0;
    					}
    					
    					for (int i = 0; i < respList.size(); i++) {
    						if(respList.get(i).getX() <= normPt.getX()) { 
    							if(normPt.getX() - respList.get(i).getX() < normPt.getX() - closestLeftPt.getX()) {
    								closestLeftPt = respList.get(i);
    								closestLeftPtInd = i;
    							}
    						}
    						if(respList.get(i).getX() > normPt.getX()) {
    							if(respList.get(i).getX() - normPt.getX() < closestRightPt.getX() - normPt.getX()) {
    								closestRightPt = respList.get(i);
    								closestRightPtInd = i;
    							}
    						}
    					}
    					
    					if(closestRightPt.equals(closestLeftPt)) {
    						closestLeftPtInd = closestLeftPtInd - 1;
    						closestLeftPt = respList.get(closestLeftPtInd);
    					}
    					
    					float deltaX = closestRightPt.getX() - closestLeftPt.getX();
    					float deltaY = closestRightPt.getY() - closestLeftPt.getY();
    					float angle = (float) Math.abs(Math.atan((double )(deltaY)/deltaX));
    					float deltaX1 = normPt.getX() - closestLeftPt.getX();
    					float h1 = (float) (deltaX1 / Math.cos((double) angle));
    					
    					float h = (float) (deltaX / Math.cos((double) angle));
    					float h2 = h - h1;
    					
    					float interpPress = (h1/h)*pressureList.get(closestLeftPtInd) + (h2/h)*pressureList.get(closestRightPtInd);
    					normalizedPressureList.add(interpPress);
    				}
    				
    				else {
    					CustomPoint closestLowerPt = respList.get(0);
    					CustomPoint closestUpperPt = respList.get(respList.size()-1);
    					int closestLowerPtInd = 0;
    					int closestUpperPtInd = respList.size() -1;
    					
    					if(respList.get(0).getY() > normPt.getY()) {
    						closestLowerPt = respList.get(respList.size() -1);
    						closestUpperPt = respList.get(0);
    						closestLowerPtInd = respList.size() -1;
    						closestUpperPtInd = 0;
    					}
    					
    					for(int i = 0; i < respList.size(); i++) {
    						if(respList.get(i).getY() <= normPt.getY()) { //if its below
    							if(normPt.getY() - respList.get(i).getY() < normPt.getY() - closestLowerPt.getY()) {
    								closestLowerPt = respList.get(i);
    								closestLowerPtInd = i;
    							}
    						}
    						if(respList.get(i).getY() > normPt.getY()) {// if its above
    							if(respList.get(i).getY() - normPt.getY() < closestUpperPt.getY() - normPt.getY()) {
    								closestUpperPt = respList.get(i);
    								closestUpperPtInd = i;
    							}
    						}
    					}
    					
    					if(closestUpperPt.equals(closestLowerPt)) {
    						closestLowerPtInd = closestLowerPtInd - 1;
    						closestLowerPt = respList.get(closestLowerPtInd);
    					}
    					
    					float deltaX = closestUpperPt.getX() - closestLowerPt.getX();
    					float deltaY = closestUpperPt.getY() - closestLowerPt.getY();
    					float angle = (float) Math.abs(Math.atan((double )(deltaX)/deltaY));
    					float deltaY1 = normPt.getY() - closestLowerPt.getY();
    					float h1 = (float) (deltaY1 / Math.cos((double) angle));
    					
    					float h = (float) (deltaY / Math.cos((double) angle));
    					float h2 = h - h1;
    					
    					float interpPress = (h1/h)*pressureList.get(closestLowerPtInd) + (h2/h)*pressureList.get(closestUpperPtInd);
    					normalizedPressureList.add(interpPress);
    					
    				}
    		}
    				
    				ArrayList<Float> movingAvg5 = simpleMovingAverage(normalizedPressureList, 5);
    				
    				String bitString = "";
    				
    				for(int i = 0; i < normalizedPressureList.size(); i++) {
    					bitString += arbiter(normalizedPressureList.get(i), movingAvg5.get(i));
    				}
    				byte [] byteArray = convBitStrToByteArr(bitString);
    				byte [] byteList = new byte[bitString.length()];
    				
    				for(int x =0; x < bitString.length(); x++) {
    					byteList[x] = Byte.parseByte(bitString.substring(x, x+1));
    				}
    				
    				deviceName = "Nexus 03";
    				testerName = "Ammar";
    				
    				String att_path = GENERATED_OUTPUT_DIR + "NormalizedStrat4/" + deviceName + "/" + testerName + "/"; 
    			    File directory = new File(att_path);
    			    if (!directory.exists()){
    			        directory.mkdirs();
    			    }
    			    att_path += "testing.txt" ;//filesList.get(a).getFileName();// + ".bin";
    			    
    			    File file = new File(att_path);
    			    file.createNewFile();
    			    
    			    try{
    			    	FileOutputStream fos = new FileOutputStream(att_path);
    			        fos.write(byteList);
    			        fos.close();
    			    }
    			    catch (IOException e){
    			        e.printStackTrace();
    			        System.exit(-1);
    			    }
    			    
    			    pressureList.clear();
    			    respList.clear();
    			    challengeList.clear();
    			    testerName = "";
    			    deviceName = "";
    			    responseStarted = false;
    			     
    	}		
    }
    
    private static ArrayList<Float> simpleMovingAverage(ArrayList<Float> inputList, int n){
    	ArrayList<Float> returnList = new ArrayList<>();
    	
    	float sum = 0;
    	
    	for(int i = 0; i < inputList.size(); i++) {
    		int floor = i - n/2;
    		if (floor < 0) {
    			floor = 0;
    		}
    		if(n%2 == 1) {
    			List<Float> splicedList;
    			if(i + n/2 > inputList.size()) {
        			splicedList = inputList.subList(floor, i);

    			}
    			else {
        			splicedList = inputList.subList(floor, i + n/2);
    			}
    	    	for (int j = 0; j < splicedList.size(); j++) {
    	    		sum+= splicedList.get(j);
    	    	}
    			returnList.add(sum / (float) splicedList.size());
    		}
    		else {
    			List<Float> splicedList = inputList.subList(floor, i + n/2);
    	    	for (int j = 0; j < splicedList.size(); j++) {
    	    		sum+= splicedList.get(j);
    	    	}
    			returnList.add(sum / (float) splicedList.size());
    		}
    	}
    	
    	return returnList;
    }
    
    private static String arbiter (float val1, float val2) {
    	return val1 > val2 ? "0": "1";
    }
    
    private static byte[] convBitStrToByteArr(String bitString) {
    	
//    	int a = Integer.parseUnsignedInt(bitString, 2);
//    	ByteBuffer bytes = ByteBuffer.allocate(2).putInt(a);
//
//    	return bytes.array();
    	
    	int byteToBuild = 0;
    	for(int i = 0; i < bitString.length(); i+=8) {
    		
    	}
    	
    	
    	return new BigInteger(bitString, 2).toByteArray();
    	
    }

}
