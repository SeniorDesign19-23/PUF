package puf.iastate.edu.puf_enrollment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;
import static java.security.AccessController.getContext;

/**
 * Created by Paul on 10/27/2015.
 * This abstracts the authentication functionality from the authenticate
 * method to create a challenge generator object for generating challenges given a seed
 */
public class ChallengeGenerator extends AppCompatActivity {
    private Random mRand;
    private long mSeed;
    private static final int SEEDLENGTH = 8;
    public static final String pufPrefs = "pufPrefs";
    private SharedPreferences prefs;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    /**
     * public constructor for a challenge generator
     * @param seed The seed for this challenge
     */
    public ChallengeGenerator(long seed){
        mRand = new Random(seed); //No seed for generating pin padding - this way two people w/ the same pin have different padding

        //Expand pin to SEEDLENGTH digits
        String seedAsString = String.valueOf(seed);
        while ( seedAsString.length() < SEEDLENGTH ) {
            seedAsString += mRand.nextInt(9); //Single digit integers only.
        }

        mSeed = Long.valueOf(seedAsString); //Set the seed

        mRand = new Random(mSeed); //Need a new random w/ this seed
    }

    public void setSeed(long seed){
        mSeed = seed;
    }

    /**
     * Public accessor for the seed
     * @return
     */
    public long getSeed(){
        return mSeed;
    }

    /**
     * Generate a challenge
     * @return A challenge vector
     */
    public ArrayList<Point> generateChallenge()
    {
        //return challGenMethod3();
        //return challGenMethod2(); //TODO: This method crashes the app
        return challGenMethod1();
    }

    private Point arragement(int index)
    {
        if(index==0)
            return randPntInBndstl();
        else if(index ==1)
            return randPntInBndstr();
        else if(index ==2)
            return randPntInBndsbl();
        else if(index ==3)
            return randPntInBndsbr();
        else
            return null;
    }

    private Point randPntInBndstl()
    {
        int x = 100+mRand.nextInt(450);
        int y = 150+mRand.nextInt(625);
        return new Point(x,y,0);
    }

    private Point randPntInBndstr()
    {
        int x = 650+mRand.nextInt(450);
        int y = 150+mRand.nextInt(625);
        return new Point(x,y,0);
    }

    private Point randPntInBndsbl()
    {
        int x = 100+mRand.nextInt(450);
        int y = 875+mRand.nextInt(625);
        return new Point(x,y,0);
    }

    private Point randPntInBndsbr()
    {
        int x = 650+mRand.nextInt(450);
        int y = 875+mRand.nextInt(625);
        return new Point(x,y,0);
    }



   /* private Point randPntInBnds()
    {
        int x = 100 + mRand.nextInt(550); // x ranges from 100 to 650
        int y = 100 + mRand.nextInt(800); // y ranges from 100 to 900
        return new Point(x, y, 0);
    }*/



    private double slope(Point p1, Point p2)
    {
        double s = (p1.y-p2.y)/(p1.x-p2.x);
        return 180*Math.atan(s)/Math.PI;
    }


    /**
     * First generation method
     * @return A challenge vector
     */
    private ArrayList<Point> challGenMethod1()
    {

        int maxX = 900;
        int maxY = 1200;


        ArrayList<Point> challenge = new ArrayList<Point>();
        // Left Upper Quadrant
        int x_min = 0;
        int x_max = maxX/2;
        int y_min = maxY/2;
        int y_max = maxY;
        //challenge.add(new Point((x_max-x_min)/2, (y_max-y_min), 0));
        challenge.add(randon_point_in_bounds(x_min, x_max, y_min, y_max));
        // Right Upper Quadrat
        x_min = maxX/2;
        x_max = maxX;
        y_min = maxY/2;
        y_max = maxY;
        //challenge.add(new Point((x_max-x_min)/2, (y_max-y_min)/2, 0));
        Point p = randon_point_in_bounds(x_min, x_max, y_min, y_max);
        challenge.add(p);
        // Right lower Quadrat
        x_min = maxX/2;
        x_max = maxX - maxX/8;
        y_min = 0;
        y_max = maxY/2;
        //challenge.add(new Point((x_max-x_min)/2, (y_max-y_min)/2, 0));
        p = randon_point_in_bounds(x_min, x_max, y_min, y_max);
        challenge.add(p);
        // Left lower Quadrat
        x_min = 0;
        x_max = maxX/2;
        y_min = 0;
        y_max = maxY/2;
        p = randon_point_in_bounds(x_min, x_max, y_min, y_max);
        challenge.add(p);
        //challenge.add(randon_point_in_bounds(x_min, x_max, y_min, y_max));

        int rejectionPoint = 50;

        return challenge;

    }

    private Point P1()
    {
        int x = 100+mRand.nextInt(500);
        int y = 150+mRand.nextInt(338);
        return new Point(x,y,0);
    }

    private Point P2()
    {
        int x = 600+mRand.nextInt(500);
        int y = 150+mRand.nextInt(338);
        return new Point(x,y,0);
    }

    private Point P3()
    {
        int x = 100+mRand.nextInt(500);
        int y = 488+mRand.nextInt(337);
        return new Point(x,y,0);
    }

    private Point P4()
    {
        int x = 600+mRand.nextInt(500);
        int y = 488+mRand.nextInt(337);
        return new Point(x,y,0);
    }

    private Point P5()
    {
        int x = 100+mRand.nextInt(500);
        int y = 825+mRand.nextInt(337);
        return new Point(x,y,0);
    }

    private Point P6()
    {
        int x = 500+mRand.nextInt(500);
        int y = 825+mRand.nextInt(337);
        return new Point(x,y,0);
    }

    private Point P7()
    {
        int x = 100+mRand.nextInt(500);
        int y = 1162+mRand.nextInt(338);
        return new Point(x,y,0);
    }

    private Point P8()
    {
        int x = 600+mRand.nextInt(500);
        int y = 1162+mRand.nextInt(338);
        return new Point(x,y,0);
    }


    /*
     * This method generates longer paths
     */
    private ArrayList<Point> challGenMethod2()
    {
        ArrayList<Point> challenge = new ArrayList<Point>();
        challenge.add(P1());
        challenge.add(P2());
        challenge.add(P3());
        challenge.add(P4());
        challenge.add(P5());
        challenge.add(P6());
        challenge.add(P7());
        int rejectionPoint = 100;

        // ensure that none of the points are too close to eachother
        boolean badPath = false;
        for (int i = 0; i < challenge.size() && !badPath; i++)
        {
            Point point1 = challenge.get(i);
            for (int j = i + 1; j < challenge.size() && !badPath; j++)
            {
                Point point2 = challenge.get(j);
                if (Math.abs((point1.x - point2.x)) < rejectionPoint
                        && Math.abs((point1.y - point2.y)) < rejectionPoint)
                {
                    badPath = true;
                }
            }
        }

        if (badPath)
        {
            return generateChallenge(); // TODO: probably the laziest way to do
            // this.
        }
        else
        {
            return challenge;
        }


    }

    /*
     * This method generates paths with only a single line
     */
    private ArrayList<Point> challGenMethod3()
    {
        ArrayList<Point> challenge = new ArrayList<>();
        challenge.add(randPntInBnds());
        challenge.add(randPntInBnds());
        int rejectionPoint = 400;

        // ensure that none of the points are too close to eachother
        boolean badPath = false;
        for (int i = 0; i < challenge.size() && !badPath; i++)
        {
            Point point1 = challenge.get(i);
            for (int j = i + 1; j < challenge.size() && !badPath; j++)
            {
                Point point2 = challenge.get(j);
                if (Math.abs((point1.x - point2.x)) < rejectionPoint
                        && Math.abs((point1.y - point2.y)) < rejectionPoint)
                {
                    badPath = true;
                }
            }
        }

        if (badPath)
        {
            return generateChallenge(); // TODO: probably the laziest way to do
            // this.
        }
        else
        {
            return challenge;
        }
    }

    /**
     * Generate a random point
     * @return a new point
     */
    private Point randPntInBnds()
    {
        int x = 100 + mRand.nextInt(550); // x ranges from 100 to 650
        int y = 100 + mRand.nextInt(800); // y ranges from 100 to 900
        return new Point(x, y, 0);
    }

    /**
     * Generate a random point with the x, y bounds
     * inclusive of both min and mox bounds
     */
    private Point randon_point_in_bounds(int x_min, int x_max, int y_min, int y_max) {
        // + 1 because java nextInt() function is exclusive of the upper bound
        int x = x_min+mRand.nextInt((x_max - x_min) + 1);
        int y = y_min+mRand.nextInt((y_max-y_min) + 1);
        return new Point(x,y,0);
    }
}
