package puf.iastate.edu.puf_enrollment;

/**
 * Created by lmand on 10/23/2018.
 */

public class Profile {
    private String profile;
    private String userName;

    public Profile(String profile, String userName){
        this.profile = profile;
        this.userName = userName;
    }


    public String getProfile() {
        return profile;
    }

    public String getUserName() {
        return userName;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
