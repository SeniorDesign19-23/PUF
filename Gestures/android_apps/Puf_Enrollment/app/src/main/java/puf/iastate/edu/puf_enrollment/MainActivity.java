package puf.iastate.edu.puf_enrollment;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import dataTypes.Challenge;
import android.graphics.Point;


public class MainActivity extends AppCompatActivity  {

    public static final String nameKeyA = "nameKeyA";
    public static final String nameKeyB = "nameKeyB";
    public static final String nameKeyC = "nameKeyC";
    public static final String nameKeyD = "nameKeyD";
    public static final String nameKeyE = "nameKeyE";
    public static final String nameKeyF = "nameKeyF";
    public static final String nameKeyG = "nameKeyG";
    public static final String nameKeyH = "nameKeyH";
    public static final String nameKeyI = "nameKeyI";
    public static final String nameKeyJ = "nameKeyJ";
    public static final String pufPrefs = "pufPrefs";
    private static final String EMAIL = "isunexus7@gmail.com";

    public LinearLayout boxA;
    public LinearLayout boxB;

    public char loadedProfile;
    private String nameA;
    private String nameB;
    private String nameC;
    private String nameD;
    private String nameE;
    private String nameF;
    private String nameG;
    private String nameH;
    private String nameI;
    private String nameJ;


    private static final String TAG = "AuthenticateActivity";
    private ArrayList<Challenge> mChallenges;

    private ProfileAdapter adapter;
    private ListView lw;
    private ArrayList<Profile> arrayOfProfiles;

    private DevicePolicyManager devicePolicyManager;
    private ComponentName componentName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        devicePolicyManager = (DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);
        componentName = new ComponentName(this, MyAdmin.class);
        // Get profile names
        SharedPreferences prefs = getSharedPreferences(pufPrefs, MODE_PRIVATE);

        arrayOfProfiles = new ArrayList<Profile>();

        adapter = new ProfileAdapter(this,arrayOfProfiles);

        lw = (ListView)findViewById(R.id.profile_list);
        lw.setAdapter(adapter);

        adapter.add(new Profile("Profile A", "No Profile "));
        adapter.add(new Profile("Profile B", "No Profile "));
        adapter.add(new Profile("Profile C", "No Profile "));
        adapter.add(new Profile("Profile D", "No Profile "));
        adapter.add(new Profile("Profile E", "No Profile "));
        adapter.add(new Profile("Profile F", "No Profile "));
        adapter.add(new Profile("Profile G", "No Profile "));
        adapter.add(new Profile("Profile H", "No Profile "));
        adapter.add(new Profile("Profile I", "No Profile "));
        adapter.add(new Profile("Profile J", "No Profile "));

        nameA = prefs.getString(nameKeyA, null);
        Profile p;
        if (nameA != null) {
            p = adapter.getItem(0);
            p.setUserName(nameA);
        }

        nameB = prefs.getString(nameKeyB, null);
        if (nameB != null) {
            p = adapter.getItem(1);
            p.setUserName(nameB);
        }
        nameC = prefs.getString(nameKeyC, null);
        if (nameC != null) {
            p = adapter.getItem(2);
            p.setUserName(nameC);
        }
        nameD = prefs.getString(nameKeyD, null);
        if (nameD != null) {
            p = adapter.getItem(3);
            p.setUserName(nameD);
        }
        nameE = prefs.getString(nameKeyE, null);
        if (nameE != null) {
            p = adapter.getItem(4);
            p.setUserName(nameE);
        }
        nameF = prefs.getString(nameKeyF, null);
        if (nameF != null) {
            p = adapter.getItem(5);
            p.setUserName(nameF);
        }
        nameG = prefs.getString(nameKeyG, null);
        if (nameG != null) {
            p = adapter.getItem(6);
            p.setUserName(nameG);
        }
        nameH = prefs.getString(nameKeyH, null);
        if (nameH != null) {
            p = adapter.getItem(7);
            p.setUserName(nameH);
        }
        nameI = prefs.getString(nameKeyI, null);
        if (nameI != null) {
            p = adapter.getItem(8);
            p.setUserName(nameI);
        }
        nameJ = prefs.getString(nameKeyJ, null);
        if (nameJ != null) {
            p = adapter.getItem(9);
            p.setUserName(nameJ);
        }

        loadedProfile = '0';
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isActive = devicePolicyManager.isAdminActive(componentName);

    }

    /**
     * Start enrollment process
     * @param v The enrollment start button
     */
    public void enroll_pressed(View v) {

        if(loadedProfile == '0') {
            Toast toast = Toast.makeText(getApplicationContext(), "please select profile", Toast.LENGTH_LONG);
            toast.show();
        } else {
            Intent enroll = new Intent(this, PinPatternGen.class);
            enroll.putExtra("profile", loadedProfile);
            startActivity(enroll);
        }
    }

    /**
     * Start example authentication process
     * @param v authentication start button
     */
    public void authenticate_pressed(View v) {
        if(loadedProfile == '0') {
            Toast toast = Toast.makeText(getApplicationContext(), "please select profile", Toast.LENGTH_LONG);
            toast.show();
        } else {
            mChallenges = new ArrayList<>();
            Gson gson = new Gson();
            SharedPreferences sharedPref = this.getSharedPreferences("puf.iastate.edu.puf_enrollment.profile", Context.MODE_PRIVATE);
            Intent authenticate = new Intent(this, RegisterGesturesActivity.class);

            try {
                String default_value = getResources().getString(R.string.profile_default_string);
                String json = "";
                switch(loadedProfile) {
                    case 'A':
                        json = sharedPref.getString(getString(R.string.profile_string_a), default_value);
                        authenticate.putExtra("name", nameA);
                        break;
                    case 'B':
                        json = sharedPref.getString(getString(R.string.profile_string_b), default_value);
                        authenticate.putExtra("name", nameB);
                        break;
                    case 'C':
                        json = sharedPref.getString(getString(R.string.profile_string_c), default_value);
                        authenticate.putExtra("name", nameC);
                        break;
                    case 'D':
                        json = sharedPref.getString(getString(R.string.profile_string_d), default_value);
                        authenticate.putExtra("name", nameD);
                        break;
                    case 'E':
                        json = sharedPref.getString(getString(R.string.profile_string_e), default_value);
                        authenticate.putExtra("name", nameE);
                        break;
                    case 'F':
                        json = sharedPref.getString(getString(R.string.profile_string_f), default_value);
                        authenticate.putExtra("name", nameF);
                        break;
                    case 'G':
                        json = sharedPref.getString(getString(R.string.profile_string_g), default_value);
                        authenticate.putExtra("name", nameG);
                        break;
                    case 'H':
                        json = sharedPref.getString(getString(R.string.profile_string_h), default_value);
                        authenticate.putExtra("name", nameH);
                        break;
                    case 'I':
                        json = sharedPref.getString(getString(R.string.profile_string_i), default_value);
                        authenticate.putExtra("name", nameI);
                        break;
                    case 'J':
                        json = sharedPref.getString(getString(R.string.profile_string_j), default_value);
                        authenticate.putExtra("name", nameJ);
                        break;
                    default:
                        json = default_value;
                        break;
                }
                mChallenges.add(gson.fromJson(json, Challenge.class));
                long pin = mChallenges.get(0).getChallengeID();

                //Pass pin to gesture training activity

                authenticate.putExtra("pin", pin);
                authenticate.putExtra("mode", "authenticate");
                authenticate.putExtra("seed", mChallenges.get(0).getChallengeID());
                authenticate.putExtra("loadedProfile", loadedProfile);
                startActivity(authenticate);

            } catch (JsonParseException e) {
                Log.e(TAG, "Error in Parsing JSON: " + e.toString());
            }
        }
    }

    /**
     * Allow user to choose profile to use
     * @param v The enrollment start button
     */
    public void grab_profile(View v) {
        Intent intent = new Intent(this, ShowProfile.class);
        startActivity(intent);
    }

    public void listerOnClick(View v, Profile p, int position) {
        v.setBackgroundColor(getResources().getColor(R.color.gray));
        loadedProfile = p.getProfile().charAt(8);
        //lw.setSelection(position);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int itemId = item.getItemId();
        if (itemId == R.id.action_settings)
        {
            // Display Settings page
            Intent preferenceIntent = new Intent(this, SettingsActivity.class);
            startActivity(preferenceIntent);
            return true;
        }
        else if (itemId == R.id.action_emailcsvs)
        {
            // Send emails
            sendCSVEmail();
            return true;
        }
        else if (itemId == R.id.clean_csvs) {
            deleteCSVs();
            return true;
        }
        else
        {
            return super.onOptionsItemSelected(item);
        }
    }

    private void sendCSVEmail()
    {
        String to = EMAIL;
        String subject = "Gesture PUF Profiles";
        String message = "";

        Intent i = new Intent(Intent.ACTION_SEND_MULTIPLE);
        i.setType("plain/text");
        i.putExtra(Intent.EXTRA_EMAIL, new String[] { to });
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT, message);

        /*
         * Date dateVal = new Date(); String filename = dateVal.toString(); data
         * = File.createTempFile("Report", ".csv"); FileWriter out =
         * (FileWriter) GenerateCsv.generateCsvFile( data, "Name,Data1");
         */

        File baseDir = new File(Environment.getExternalStorageDirectory(),
                "UD_PUF");
        if (!baseDir.exists())
        {
            baseDir.mkdirs();
        }
        List<File> files = getListFiles(baseDir);

        startActivity(Intent.createChooser(i, "E-mail"));

        ArrayList<Uri> uris = new ArrayList<Uri>();
        // convert from paths to Android friendly Parcelable Uri's
        for (File fileIn : files)
        {
            Uri u = Uri.fromFile(fileIn);
            uris.add(u);
        }
        i.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        startActivity(Intent.createChooser(i, "Send mail..."));
    }

    private List<File> getListFiles(File parentDir)
    {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        for (File file : files)
        {
            if (file.isDirectory())
            {
                inFiles.addAll(getListFiles(file));
            }
            else
            {
                if (file.getName().endsWith(".csv"))
                {
                    inFiles.add(file);
                }
            }
        }
        return inFiles;
    }

    // Deletes all files in the UD_PUF directory
    private void deleteCSVs () {
        File dir = new File(Environment.getExternalStorageDirectory(), "UD_PUF");
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }
        Toast toast = Toast.makeText(getApplicationContext(), "Saved Profiles Deleted", Toast.LENGTH_LONG);
        toast.show();
    }


    /**
     * start the super secret activity
     */
    public void secret_pressed(View v){
        Intent intent = new Intent(this, SecretAcrivity.class);
        startActivity(intent);
    }

    public void normalize_test_pressed(View v){
        Intent intent = new Intent(this, NormalizeTestActivity.class);
        startActivity(intent);
    }



}
