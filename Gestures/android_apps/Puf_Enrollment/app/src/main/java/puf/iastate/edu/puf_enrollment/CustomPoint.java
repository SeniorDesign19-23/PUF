
public class CustomPoint {

	float x;
	float y;
	
	public CustomPoint(float xCoord, float yCoord) {
		x = xCoord;
		y = yCoord;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
}
