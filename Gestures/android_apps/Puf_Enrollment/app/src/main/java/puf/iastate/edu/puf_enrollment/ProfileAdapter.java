package puf.iastate.edu.puf_enrollment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextClock;
import android.widget.TextView;

import java.util.List;

/**
 * Created by lmand on 10/23/2018.
 */
public class ProfileAdapter extends ArrayAdapter<Profile> {
    private Context context;
    private int resource;
    private List<Profile> profiles;
    private int CurrPosition;
    private List<Profile> ori;
    private int previousSpot = -1;

    public ProfileAdapter(@NonNull Context context, List<Profile> profiles) {
        super(context, 0, profiles);

        this.context = context;
        this.profiles = profiles;
        this.ori = profiles;

    }

    @Override
    public int getCount(){
        return profiles.size();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent){
        Profile p = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_profile, parent, false);
        }
        TextView name = (TextView)convertView.findViewById(R.id.userName);
        TextView profile = (TextView)convertView.findViewById(R.id.profileName);

        name.setText(p.getUserName());
        profile.setText(p.getProfile());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0; i < parent.getChildCount(); i++){
                    parent.getChildAt(i).setBackgroundColor(context.getResources().getColor(R.color.white));
                }
                MainActivity m = (MainActivity)context;
                m.listerOnClick(v, getItem(position), position);
            }
        });

        return convertView;


    }

}
